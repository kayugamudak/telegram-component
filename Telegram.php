<?php

namespace common\components;

use common\helpers\IdHelper;
use common\models\telegram\TelegramUser;
use common\models\telegram\TelegramWebmasterChat;
use yii\base\Component;

/**
 * Get chat ids:
 * @see https://api.telegram.org/bot[token]/getUpdates
 *
 * Class Telegram
 * @package common\components
 */
class Telegram extends Component
{
    private const PARSE_MODE_HTML = 'html';

    /**
     * @var string
     */
    public $token;

    /**
     * @var string
     */
    private $telegramUrl = 'https://api.telegram.org';

    /**
     * @var string
     */
    private $telegramJoinUrl = 'https://t.me/';

    /**
     * @var string
     */
    public $botLogin = 'BotLogin';


    /**
     * Вебмастер - это пользователь нашей системы
     * Отправка сообщения пользователю в чат с ботом
     * @param $message
     * @param $chatId
     * @return bool
     */
    public function sendMessage($message, $chatId): bool
    {
        $url = $this->telegramUrl . '/bot' . $this->token . '/sendMessage';
        $data = [
            'chat_id' => $chatId,
            'parse_mode' => self::PARSE_MODE_HTML,
            'text' => $message
        ];

        $response = \Yii::$app->httpClient->get($url, $data)->send();
        return $response->isOk;
    }


    /**
     * Авторизуем пользователя следующим образом: создаем ему уникальный uuid и даем ссылку https://t.me/LeadsTechBot?start=<uuid>
     * При переходе по ссылке пользователь автоматически пошлёт в чат с ботом сообщение /start <uuid>
     * Далее при разборе вебхука мы сможем идентифицировать пользователя и связать id чата в терминах телеграма с id пользователя у нас
     *
     * @param int $webmasterId
     * @return string
     */
    public function getWebmasterAuthLink(int $webmasterId) {
        $webmasterChat = TelegramWebmasterChat::find()
            ->where(['webmasterId' => $webmasterId])
            ->one();
        if ($webmasterChat === null) {
            $webmasterChat = new TelegramWebmasterChat();
            $webmasterChat->authToken = IdHelper::guidv4();
            $webmasterChat->webmasterId = $webmasterId;
            $webmasterChat->save();
        }
        return $this->telegramJoinUrl . $this->botLogin . '?start=' . $webmasterChat->authToken;
    }


    /**
     * Принимаем 2 вида вебхуков:
     * /start <uuid> для авторизации
     * /do magic - пользователь создает вручную чат, добавляет туда нашего бота, шлёт эту команду и узнаёт ID этого чата,
     * и затем мы можем прописать этот ID у пользователя, и бот начнёт слать сообщения для этого пользователя в этот чат
     * @param $data
     */
    public function processWebhook($data) {
        if (!isset($data['message'])) {
            return;
        }
        $message = $data['message'];

        if (isset($message['from']['id'])) {
            $telegramUser = TelegramUser::findOne(['id' => $message['from']['id']]);
            if ($telegramUser === null) {
                $telegramUser = new TelegramUser();
            }
            $telegramUser->load($message['from'],'') && $telegramUser->save();
        }

        if (isset($message['text']) && isset($message['chat']['id'])) {

            $regex = '/(\/start\s)(\w+)/';
            if ( preg_match($regex, $message['text'], $match)) {
                $authToken = $match[2] ?? null;
                if ($authToken) {
                    $telegramWebmasterChat = TelegramWebmasterChat::findOne(['authToken' => $authToken]);
                    if ($telegramWebmasterChat !== null) {
                        $telegramWebmasterChat->chatId = $message['chat']['id'];
                        $telegramWebmasterChat->save();
                    }
                }
                return;
            }

            $regex = '/(\/do\s)(\w+)/';
            if ( preg_match($regex, $message['text'], $match)) {
                $command = $match[2] ?? null;
                switch($command) {
                    case 'magic': {
                        \Yii::$app->telegram->sendMessage($message['chat']['id'], $message['chat']['id']);
                        break;
                    }
                    default: {
                        break;
                    }
                }
                return;
            }
        }
    }
}
