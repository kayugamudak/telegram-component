<?php

namespace common\components\alert\channels;

use common\models\telegram\TelegramWebmasterChat;
use common\models\Webmaster;

class TelegramChannel implements ChanelInterface {

    public const VIEW_PATH = '@common/components/alert/views/telegram/';

    public function sendMessage(string $view, array $params, $chatId = null, ?Webmaster $webmaster = null)
    {
        if (!$chatId) {
            $telegramWebmasterChat = TelegramWebmasterChat::find()
                ->andWhere(['webmasterId' => $webmaster->id])
                ->andWhere(['is', 'chatId', new \yii\db\Expression('not null')])
                ->one();
            if ($telegramWebmasterChat === null) {
                return false;
            }
            $chatId = $telegramWebmasterChat->chatId;
        }
        if ($chatId) {
            \Yii::$app->telegram->sendMessage($this->renderMessage($view, $params), $chatId);
        }
    }

    public function renderMessage(string $view, array $params) {
        $viewPath = self::VIEW_PATH . $view . '.php';
        return \Yii::$app->view->renderFile($viewPath, $params);
    }
}