<?php

namespace common\components\alert\channels;

interface ChanelInterface {
    public function sendMessage();
}